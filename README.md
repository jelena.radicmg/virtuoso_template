# BAG2 Virtuoso Environment

TL;DR
Git submodules are added with  ssh URL's. I strongly advice you to execute

"eval `ssh-agent -c`; ssh-add"
(Those backticks are really needed.)

to avoid typing your passphrase.

First:
`./init_submodules.sh`

To install python dependencies:
`./pip3userinstall.sh`

To initialize the invironment
`source sourceme.csh`

Once the setup is OK
`virtuoso &`

To generate an inverter
`./configure`
(refresh virtuoto)
`make gen inverter_gen`

## Documentation
Documentation of MOSAIC BAG2 virtuoso template can be found at

[https://mosaic_group.gitlab.io/mosaic_BAG/virtuoso_template/](https://mosaic_group.gitlab.io/mosaic_BAG/virtuoso_template/)

