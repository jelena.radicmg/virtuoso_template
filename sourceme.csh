#Sourceme template for a BAG2 project
setenv VIRTUOSO_DIR   "`pwd`"

if ( "${VIRTUOSO_DIR}" == "${HOME}" ) then
 echo "Do not run this script in your home directory."
 echo "Configuration files in home directory may mess up your configuration glob
ally."
 exit 1
endif

# Collect environment variables to the beginnning of the file
# PDK_HOME and TECHLIB are ued by BAG2 YOU MUST DEFINE THEM
#setenv PDK_HOME "Path to your PDK home"
#setenv TECHLIB "Name of the virtuoso technology library"
####



#Copy essential files to the project directory
# I do not know where you have them, but you must provide them
# Example:
#set FILELIST=".cdsinit CCSinvokeCdfCallbacks.il"
#set FILESOURCE="This is from where you can copy the necessary files to this directory"
#foreach file ( ${FILELIST} )  
#  if ( ! -f $file ) then
#    echo "Copying file ${FILESOURCE}/${file} to ./${file}"
#    cp ${FILESOURCE}/${file} ./
#  else if (  !="" ) then
#    echo "System wide ${FILESOURCE}/${file} is newer than ./${file}"
#    echo "Please check your configuration *** "
#    echo "Please check your configuration *** "
#    echo "Please check your configuration"
#  endif
#end

### Here I will do some checks for you tso you do not screw up
if ( ! $?PDK_HOME ) then
    echo "PDK_HOME not set"
    echo "In this file you MUST set your environment setup so that you have all tools available and PDK set up correctly"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    exit
endif

if ( ! $?TECHLIB ) then
    echo "TECHLIB not set"
    echo "It is NOT taken care for you by this file"
    echo "Exiting"
    exit
endif


if (  `which virtuoso` == "" ) then
    echo "FATAL: Virtuoso not in path"
    echo "Exiting"
    exit
endif

if ( ! -f ./cds.lib.core ) then
    echo "You must provide cds.lib.core"
    echo "cds.lib.core should define general and PDK libraries"
    echo "Exiting"
    exit
endif
if ( ! -f ./.cdsinit ) then
    echo "You must provide ./.cdsinit"
    echo "Usually provided by your PDK vendor"
    echo "Exiting"
    exit
endif

if ( ! -f ./cds.lib ) then
    echo "You do not have  ./.cds.lib"
    echo "I will create one for you"
cat << EOF > ./cds.lib
SOFTINCLUDE ./cds.lib.bag
SOFTINCLUDE ./cds.lib.core
EOF

endif


## Softinclude BAG2 structured library definitions
if ( `grep "SOFTINCLUDE ./cds.lib.core" ./cds.lib` == "" ) then
    echo "Adding cds.lib.core to cds.lib"
    sed -i '1s#^#SOFTINCLUDE ./cds.lib.core\n#' ./cds.lib 
endif

if ( `grep "SOFTINCLUDE ./cds.lib.bag" ./cds.lib` == "" ) then
    echo "Adding cds.lib.bag to cds.lib"
    sed -i '1s#^#SOFTINCLUDE ./cds.lib.bag\n#' ./cds.lib 
endif

# Check for user customization section in .cdsinit
if ( `grep ";END OF USER CUSTOMIZATION" ./.cdsinit` == "" ) then
    chmod 750 ./.cdsinit
    echo ";END OF USER CUSTOMIZATION" >> ./.cdsinit
endif

if ( `grep 'load("start_bag.il")' ./.cdsinit` == "" ) then
    echo "Adding start_bag.il to .cdsinit"
    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("start_bag.il"\)\n\1/g' ./.cdsinit
endif
#
if ( ! -f ./CCSinvokeCdfCallbacks.il ) then
    echo "You do not have CCSinvokeCdfCallbacks.il"
    echo "It is mandatory if you use parametrized primitives with BAG"
    echo "Download it from here:" 
    echo "https://support.cadence.com/apex/ArticleAttachmentPortal?id=a1Od0000000namyEAA&pageName=ArticleContent"
    exit
endif

if ( `grep 'load("CCSinvokeCdfCallbacks.il")' ./.cdsinit` == "" ) then
    echo "Adding CCSinvokeCdfCallbacks.il to .cdsinit"
    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("CCSinvokeCdfCallbacks.il"\)\n\1/g' ./.cdsinit
endif

if ( ! -f ./bag_libs.def ) then
    echo "./bag_libs.def does not exist. Creating it for you"
    echo 'BAG_prim ${BAG_TECH_CONFIG_DIR}/Tech_primitives'  > ./bag_libs.def
endif

## This might be needed
#if ( -z $(grep CCSCdfCallbackEntireLib.il ./.cdsinit) ) then
#    sed -i 's/\(^ *;END OF USER CUSTOMIZATION *$\)/load\("CCSCdfCallbackEntireLib.il"\)\n\1/g' ./.cdsinit
#endif

# This takes ecd_bag helpers into use
if ( ! $?PYTHONPATH ) then
    setenv PYTHONPATH ${VIRTUOSO_DIR}:${VIRTUOSO_DIR}/bag_ecd
else
    setenv PYTHONPATH ${PYTHONPATH}:${VIRTUOSO_DIR}:${VIRTUOSO_DIR}/bag_ecd
endif

### 'Standard' setup for BAG
# set BAG working directory
setenv BAG_WORK_DIR ${VIRTUOSO_DIR}
# set BAG framework directory
setenv BAG_FRAMEWORK "${BAG_WORK_DIR}/BAG2_framework"
# set BAG python executable
setenv BAG_PYTHON "python3"
# set jupyter notebook path
#setenv BAG_JUPYTER "${HOME}/.local/bin/jupyter-notebook"
# set technology/project configuration directory
setenv BAG_TECH_CONFIG_DIR "${BAG_WORK_DIR}/BAG2_technology_definition"
# set where BAG saves temporary files
setenv BAG_TEMP_DIR "${BAG_WORK_DIR}/BAGTMP"
# set location of BAG configuration file
setenv BAG_CONFIG_PATH "${BAG_WORK_DIR}/bag_config.yaml"

## set IPython configuration directory
##setenv IPYTHONDIR "$BAG_WORK_DIR/.ipython"

echo "Run 'virtuoso'"
echo "To configure bag project for compilations, run ./configure"

